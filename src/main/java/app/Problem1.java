package app;

import java.math.BigInteger;

/**
 * Created by Ammar on 2/10/2018.
 */
public class Problem1 {


    public static void main(String args[]) {

        BigInteger nthValue = BigInteger.valueOf(0);

        int n = 1000;
        for(int i = 1 ; i <= n ; i++) {

            //for every even number(i): multiply the i with the nth value of n-1
            if (i % 2 == 0) {
                nthValue = nthValue.multiply(BigInteger.valueOf(i));
            }
            //for every odd number(i): add the i with the nth value of n-1
            else {
                nthValue = nthValue.add(BigInteger.valueOf(i));
            }

            System.out.println("n: " + i + " nth value = " + nthValue);
        }
    }
}
