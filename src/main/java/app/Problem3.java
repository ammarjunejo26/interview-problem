package app;

/**
 * Created by Ammar on 2/10/2018.
 */
public class Problem3 {

    private static final String alphabetStringUpperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String alphabetStringLowerCase = "abcdefghijklmnopqrstuvwxyz";

    //set the offest to right shift of 5 to decrypt the text in problem 2
    private static final int offset = 5;

    //this is the encrypted text from problem 2
    private static String textToDecrypt = "Vgdxzrvnwzbdiidibojbzoqzmtodmzyjandoodibwtczmndnozmjioczwvifviyjacvqdibijo" +
            "cdibojyjjixzjmordxznczcvykzzkzydiojoczwjjfczmndnozmrvnmzvydibwpodocvyijkdxopmznjmxjiqzmnvodjindidoviyrcvodnocz" +
            "pnzjavwjjfocjpbcoVgdxzrdocjpokdxopmznjmxjiqzmnvodjiNjnczrvnxjindyzmdibdiczmjrihdiyvnrzggvnnczxjpgyajmoczcjoyvt" +
            "hvyzczmazzgqzmtngzzktviynopkdyrczoczmoczkgzvnpmzjahvfdibvyvdntxcvdirjpgywzrjmococzomjpwgzjabzoodibpkviykdxfdib" +
            "oczyvdndznrczinpyyzigtvRcdozMvwwdordockdifztznmvixgjnzwtczm";

    public static void main(String args[]) {

        char[] textToEncryptArray = textToDecrypt.toCharArray();

        String decryptedText = "";

        for (Character character: textToEncryptArray) {
            int newIndex;
            if(checkUpperCase(character)){
                int index = alphabetStringUpperCase.indexOf(character);
                newIndex = findNewIndex(index);
                decryptedText += alphabetStringUpperCase.charAt(newIndex);
            }
            else{
                int index = alphabetStringLowerCase.indexOf(character);
                newIndex = findNewIndex(index);
                decryptedText += alphabetStringLowerCase.charAt(newIndex);
            }
        }
        System.out.println("Decrypted Text : " + decryptedText);
    }

    private static Boolean checkUpperCase(final Character character){

        String value = character.toString().toUpperCase();
        if(value.equals(character.toString())){
            return true;
        }
        else{
            return false;
        }

    }
    private static int findNewIndex(final int index){

        int newIndex = index + offset;

        if (newIndex <0){
            newIndex = newIndex + 26;
        }
        else if(newIndex > 25 ){
            newIndex = newIndex - 26;
        }
        return newIndex;
    }


}
