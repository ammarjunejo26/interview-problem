package app;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ammar on 2/10/2018.
 */
public class Problem4 {

    public static void main(String args[]) {

        String input = "abcdaabcdeabaaacbfaaaabcab 123aabcaabca35aa";
        char[] inputArray = input.toCharArray();
        StringBuilder updateInput = new StringBuilder(input);

        //list to store the index of characters to replace with #
        List<Integer> listOfIndexesToChangedToHash  = new ArrayList<>();
        //list to store the index of characters to replace with $
        List<Integer> listOfIndexesToChangedToDollar  = new ArrayList<>();

        //for loop the characters of the text provided
        for (int i = 0 ; i < inputArray.length ; i++) {

            if(i == 0){
                if(Character.toString(input.charAt(i)).equalsIgnoreCase("a") &&  !Character.toString(input.charAt(i + 1)).equalsIgnoreCase("a")){
                    //if its an a single a on its own then add the index of this a to the list : listOfIndexesToChangedtoHash
                    listOfIndexesToChangedToHash.add(i);
                }
            }
            else{
                //if the character is a and it is not after an a, then add it to the list : listOfIndexesToChangedtoDollar
                if (Character.toString(input.charAt(i)).equalsIgnoreCase("a") && Character.toString(input.charAt(i - 1)).equalsIgnoreCase("a")) {
                    listOfIndexesToChangedToDollar.add(i);
                }
                //else if the character is 'a' and not followed by an 'a', the add it to the list : listOfIndexesToChangedtoHash
                else if (Character.toString(input.charAt(i)).equalsIgnoreCase("a") && !Character.toString(input.charAt(i + 1)).equalsIgnoreCase("a")) {
                    listOfIndexesToChangedToHash.add(i);
                }
            }
        }

        System.out.println("Original input : " + input);

        //update all the characters with # and $
        for(Integer integer : listOfIndexesToChangedToHash){
            updateInput.setCharAt(integer, '#');
        }
        for(Integer integer : listOfIndexesToChangedToDollar){
            updateInput.setCharAt(integer, '$');
        }

        System.out.println("Updated text: " + updateInput);
    }
}
