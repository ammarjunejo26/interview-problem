package app;

/**
 * Created by Ammar on 2/10/2018.
 */
public class Problem2and3 {

    private static final String alphabetStringUpperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String alphabetStringLowerCase = "abcdefghijklmnopqrstuvwxyz";

    public static void main(String args[]) {

        String textToEncrypt = "Alicewasbeginningtogetverytiredofsittingbyhersisteronthebankandofhavingnothingt" +
                "odoonceortwiceshehadpeepedintothebookhersisterwasreadingbutithadnopicturesor" +
                "conversationsinitandwhatistheuseofabookthoughtAlicewithoutpicturesorconversati" +
                "onSoshewasconsideringinherownmindaswellasshecouldforthehotdaymadeherfeelve" +
                "rysleepyandstupidwhetherthepleasureofmakingadaisychainwouldbeworththetroubl" +
                "eofgettingupandpickingthedaisieswhensuddenlyaWhiteRabbitwithpinkeyesranclose" +
                "byher";


        //set the offset to left shift of 5
        int offset = -5;
        String encryptedText = process(textToEncrypt , offset);
        System.out.println("Encrypted Text : " + encryptedText);

        //set the offset with right shift of 5 to get back the original text
        offset = 5;
        String decryptedText = process(encryptedText, offset);
        System.out.println("Decrypted Text : " + decryptedText);

    }

    private static String process(final String text, final int offset){

        char[] textToEncryptArray = text.toCharArray();

        String encryptedText = "";

        //for loop each character in the text provided
        for (Character character: textToEncryptArray) {
            int newIndex;
            if(checkUpperCase(character)){
                int index = alphabetStringUpperCase.indexOf(character);
                newIndex = findNewIndex(index,offset);
                encryptedText += alphabetStringUpperCase.charAt(newIndex);
            }
            else{
                int index = alphabetStringLowerCase.indexOf(character);
                newIndex = findNewIndex(index,offset);
                encryptedText += alphabetStringLowerCase.charAt(newIndex);
            }
        }
        return encryptedText;
    }

    private static Boolean checkUpperCase(final Character character){

        String value = character.toString().toUpperCase();
        if(value.equals(character.toString())){
            return true;
        }
        else{
            return false;
        }

    }

    private static int findNewIndex(final int index, final int offset){

        int newIndex = index + offset;

        if (newIndex <0){
            newIndex = newIndex + 26;
        }
        else if(newIndex > 25 ){
            newIndex = newIndex - 26;
        }
        return newIndex;
    }
}
